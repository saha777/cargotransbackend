namespace CargoTransBackend.Models
{
    public class Car
    {
        public int Id { get; set; }
        public int Makes { get; set; }
        public string Model { get; set; }
        public float MaxCargoVolume { get; set; }
        public float MaxCargoMass { get; set; }
    }
}