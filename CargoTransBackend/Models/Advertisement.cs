using System;
using System.Collections.Generic;

namespace CargoTransBackend.Models
{
    public class Advertisement
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Location StartPoint { get; set; }
        public Location EndPoint { get; set; }
        public DateTime OrientedDateFrom { get; set; }
        public DateTime OrientedDateTo { get; set; }
        public float CargoVolume { get; set; }
        public string Description { get; set; }
        public User Owner { get; set; }
        public Request ChoosedRequest { get; set; }
        public List<Request> Requests { get; set; }
    }
}