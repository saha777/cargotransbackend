using System.Collections.Generic;

namespace CargoTransBackend.Models
{
    public class User : UserCredentials
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public List<Contact> Contacts { get; set; }
    }
}