using System;
using System.Collections.Generic;

namespace CargoTransBackend.Models
{
    public class Driver
    {
        public DateTime Birthday { get; set; }
        public float Rating { get; set; }
        public int TransactionsNumber { get; set; }
        public string Description { get; set; }
        public List<Car> Cars { get; set; }
    }
}