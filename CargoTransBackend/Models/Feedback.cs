namespace CargoTransBackend.Models
{
    public class Feedback
    {
        public int Id { get; set; }
        public float Rating { get; set; }
        public string Description { get; set; }
        public User User { get; set; }
        public Advertisement Advertisement { get; set; }
    }
}