namespace CargoTransBackend.Models
{
    public class Request
    {
        public int Id { get; set; }
        public float OrientedCost { get; set; }
        public string Description { get; set; }
        public Driver Driver { get; set; }
        public Advertisement Advertisement { get; set; }
    }
}