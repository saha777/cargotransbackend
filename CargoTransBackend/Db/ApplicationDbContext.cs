
using Microsoft.EntityFrameworkCore;

namespace CargoTransBackend.Db
{
    public class ApplicationDbContext : DbContext
    {
        protected string ConnectionString { get; }

        public ApplicationDbContext(string connectionString)
        {
            ConnectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL(ConnectionString);
        }
    }
}